# Library
Basic library RESTful API.

## Description
* Uses Flask framework.
* Supports CRUD operations to manage data resources.
* Resources are managed in example SQLite database. Creation and updates of resources are timestamped.
* Consumes and produces _application/json_ content.
* Logs _werkzeug_, _sqlalchemy_ messages and API responses. Configurable in _/server/logconfig.json_.
* Uses HTTP Basic Auth for first time authentication, and issues timed token to be used for subsequent requests.
* Does simple validation on input data.

## Required packages:
* Flask
* Flask-SQLAlchemy
* Flask-Migrate
* Flask-Script
* Marshmallow
* Flask-Marshmallow
* Marshmallow-SQLAlchemy
* PyJWT


## API endpoints
POST, GET, PUT, DELETE methods for create, read, update and delete. 
<br> Required and optional refer to __field names of JSON body__ when creating or updating resource.

* **_/api/books/{id}_**
<br>Book resources
<br>**Required:** title, publisher_id
<br>**Optional:** copies

* **_/api/borrowers/{id}_**
<br>Borrower resources
<br>**Required:** first_name, last_name, address, phone, email

* **_/api/bookborrowers/{id}_**
<br>Book-Borrower relations. Used to check-in and check-out books.
<br>**Required:** book_id, borrower_id
<br>**Optional:** returned

* **_/api/publishers/{id}_**
<br>Publisher resources
<br>**Required:** name

* **_/api/authors/{id}_**
<br>Author resources
<br>**Required:** first_name, last_name

* **_/api/bookauthors/{id}_**
<br>Book-Author relations
<br>**Required:** book_id, author_id

* **_/auth/token/_**
<br>Generate token for credentials, where username and password must be sent in request header. Only GET, POST.

* **_/auth/register/_**
<br> Register new user. Only POST.
<br>**Required:** username, password
<br>*First user requires _roles_

* **_/auth/unregister/_**
<br> Delete user. Admin or self required. Only DELETE.

* **_/auth/edit/{id}_**
<br> Edit user details. Admin or self required. Only PUT.

* **_/auth/users/{id}_**
<br> Users index/details. Only GET.


## Filtering, sorting
### Filtering
**filter=**_attribute_**:**_operator_**:**_other[,other,...][_**;**_attribute_**:**_operator_**:**_other[,other,...]_**;**_...]_
````
http://localhost:5555/api/bookborrowers?filter=deadline_on:gt:$_date.2019.12.26.00.39
http://localhost:5555/api/bookborrowers?filter=deadline_on:gt:$_now
http://localhost:5555/api/bookborrowers?filter=created_on:eq:$updated_on
````
__Advanced note.__ Can also chain multiple __filter__ params. By default these are treated as disjunction, 
where each param is joined by __OR__ and each subgroup in one param separated by __;__ is joined by __AND__. 
When specifying param __filter_opt=cnf__, then the whole thing is treated as conjunction.

#### Attribute, Other
_Attribute_ is resource attribute. _Other_ can be some value or another attribute prefixed with $ or specific value like $_null, $_now or $_date. Date elements
should be separated by dot, like $_date.2019.12.26.00.39 ($_date.year.month.day.hours.minutes).

#### Operators
* **eq** - equal
* **ne** - not equal
* **ge** - greater or equal
* **le** - less or equal
* **gt** - greater than
* **lt** - less than
* **in** - in comma separated values
* **between** - between two values separated by comma
* **notin** - not in comma separated values
* **contains** - 
* **startswith** - 
* **endswith** -

### Sorting
**sort**=_attribute_**:**_direction_[;_attribute_:_direction_;...]
````
http://localhost:5555/api/books?sort=year:dsc
````
Direction can be either '_asc_' or '_dsc_'.

## Sample JSON output

General response format:
```json
{
    "success": false,
    "status": "401 UNAUTHORIZED",
    "message": "No permission -- see authorization schemes",
    "data": "Token signature expired. Please log in again."
}
```
In case of validation errors:
```json
{
    "success": false,
    "status": "400 BAD_REQUEST",
    "message": "Bad request syntax or unsupported method",
    "data": {
        "borrower_id": [
            "Missing data for required field."
        ],
        "borrower": [
            "Unknown field."
        ],
        "book_id": [
            "Out of stock"
        ]
    }
}
```
GET _bookborrowers_
```json
{
    "success": true,
    "status": "200 OK",
    "message": "Request fulfilled, document follows",
    "data": [
        {
            "book": {
                "title": "Universumi MIKROMAAILM",
                "id": 1
            },
            "borrower": {
                "first_name": "John",
                "id": 1,
                "last_name": "Doe"
            },
            "id": 1,
            "created_on": "2019-12-18T23:26:01",
            "updated_on": "2019-12-18T23:26:01",
            "deadline_on": "2019-12-25",
            "returned_on": null,
            "returned": false
        },
        {
            "book": {
                "title": "Universum valguses ja vihmas",
                "id": 2
            },
            "borrower": {
                "first_name": "Jane",
                "id": 2,
                "last_name": "Doe"
            },
            "id": 2,
            "created_on": "2019-12-19T00:39:47",
            "updated_on": "2019-12-19T00:39:47",
            "deadline_on": "2019-12-26",
            "returned_on": null,
            "returned": false
        }
    ]
}
```

## SQL migration commands.
* Initialize migration environment:
<br>_python manage.py db init_

* Create new version based on detected changes:
<br>_python manage.py db migrate_

* Upgrade to next version:
<br>_python manage.py db upgrade_

* Downgrade to previous version:
<br>_python manage.py db downgrade_


## TODO:
* Pagination
* Tests
* Digest Auth support
* Some simple web UI
