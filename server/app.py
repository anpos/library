"""
This script runs the application using a development server.
It contains the definition of routes and views for the application.
"""

from libapi import create_app
import json
import logging.config

app = create_app()

# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app


if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555

    # path = os.path.dirname(os.path.realpath(__file__)) + '/repository/logging.json'
    file = open('logconfig.json', 'r')
    if file is not None:
        logging_config = json.load(file)
        logging.config.dictConfig(logging_config)
        file.close()

    app.run(HOST, PORT)
