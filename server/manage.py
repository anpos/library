﻿"""
manage.py
- provides a command line utility for interacting with the application to perform interactive debugging and setup.

python manage.py db init

Create new version based on detected changes:
 python manage.py db migrate

Upgrade to next version:
 python manage.py db upgrade

Downgrade to previous version:
 python manage.py db downgrade

Note: Dropping with SQLite: migrations/env.py: context.configure(render_as_batch=True, ...) in run_migrations_online()
"""


from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from libapi import create_app
from libapi.models import *


app = create_app()

migrate = Migrate(app, db)
manager = Manager(app)

# provide a migration utility command
manager.add_command('db', MigrateCommand)

# enable python shell with application context
@manager.shell
def shell_ctx():
    return dict(app=app,
                db=db,
                User=User,
                Publisher=Publisher,
                Author=Author,
                Borrower=Borrower,
                Book=Book,
                BookAuthor=BookAuthor,
                BookBorrower=BookBorrower)


if __name__ == '__main__':
    manager.run()
