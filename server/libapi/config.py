﻿"""
config.py
- settings for the flask application object

dialect+driver://username:password@host:port/database

https://docs.sqlalchemy.org/en/13/core/engines.html
"""


class BaseConfig(object):
    # DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///library.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'my-secret-key'
    JSON_SORT_KEYS = False


class ApiConfig(object):
    # Determines how long authorization lasts in milliseconds
    API_TOKEN_LIFETIME = 3600

    # Standard borrowing time in days
    API_STANDARD_BORROW_TIME = 28

    # Shortened borrowing time in days
    API_SHORTENED_BORROW_TIME = 7

    # Min limit to use standard borrowing time
    API_MIN_BOOK_COPIES = 5

    # Min age in months to use standard borrowing time
    API_MIN_BOOK_AGE = 3
