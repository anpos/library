﻿"""
application.py
- creates a Flask app instance and registers the database object
"""

from flask import Flask

from sqlalchemy.engine import Engine
from sqlalchemy import event


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    """Turn on foreign key constraints in SQLite 3.x"""
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


def create_app(app_name='LIBRARY_API'):
    app = Flask(app_name)
    app.config.from_object('libapi.config.BaseConfig')
    app.config.from_object('libapi.config.ApiConfig')

    from .restful import handle_bad_request
    from werkzeug.exceptions import BadRequest
    app.register_error_handler(BadRequest, handle_bad_request)

    from .api import api, auth
    app.register_blueprint(api, url_prefix="/api")
    app.register_blueprint(auth, url_prefix="/auth")

    from .models import db, ma
    db.init_app(app)
    ma.init_app(app)

    return app
