﻿"""
api.py
- provides the API endpoints for consuming and producing REST requests and responses
"""

from .auth import *


api = Blueprint('api', __name__)


@api.route('/authors/', methods=['POST', 'GET'])
@api.route('/authors/<int:key>', methods=['GET', 'PUT', 'DELETE'])
@consumes_json
@restful_response
@authorize(R.ADMIN, GET=R.ANY_USER)
def handle_authors(current_user, key=None):
    return crud_operation[request.method](authors_schema, key)


@api.route('/publishers/', methods=['POST', 'GET'])
@api.route('/publishers/<int:key>', methods=['GET', 'PUT', 'DELETE'])
@consumes_json
@restful_response
@authorize(R.ADMIN, GET=R.ANY_USER)
def handle_publishers(current_user, key=None):
    return crud_operation[request.method](publishers_schema, key)


@api.route('/borrowers/', methods=['POST', 'GET'])
@api.route('/borrowers/<int:key>', methods=['GET', 'PUT', 'DELETE'])
@consumes_json
@restful_response
@authorize(R.ANY_USER)
def handle_borrowers(current_user, key=None):
    return crud_operation[request.method](borrowers_schema, key)


@api.route('/books/', methods=['POST', 'GET'])
@api.route('/books/<int:key>', methods=['GET', 'PUT', 'DELETE'])
@consumes_json
@restful_response
@authorize(R.ADMIN, GET=R.ANYONE)
def handle_books(current_user, key=None):
    if current_user is None:
        return crud_operation[request.method](books_schema_no_borrowers, key)
    return crud_operation[request.method](books_schema, key)


@api.route('/bookauthors/', methods=['POST', 'GET'])
@api.route('/bookauthors/<int:key>', methods=['GET', 'PUT', 'DELETE'])
@consumes_json
@restful_response
@authorize(R.ADMIN, GET=R.ANY_USER)
def handle_bookauthors(current_user, key=None):
    return crud_operation[request.method](bookauthors_schema, key)


@api.route('/bookborrowers/', methods=['POST', 'GET'])
@api.route('/bookborrowers/<int:key>', methods=['GET', 'PUT', 'DELETE'])
@consumes_json
@restful_response
@authorize(R.ANY_USER)
def handle_bookborrowers(current_user, key=None):
    return crud_operation[request.method](bookborrowers_schema, key)
