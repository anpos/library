"""
query_utils.py
- Filtering, sorting and pagination

https://docs.sqlalchemy.org/en/13/orm/internals.html
"""

from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.relationships import RelationshipProperty
from sqlalchemy import and_, or_, asc, desc
from datetime import datetime, timedelta
from .restful import QueryError


class QueryBase:
    """Base."""

    relation_separator = '.'

    @classmethod
    def _parse_attribute(cls, model, name: str):
        names = name.split(cls.relation_separator, 1)
        related = None
        if len(names) == 2:
            name, related = names
        attr = getattr(model, name, None)
        if not isinstance(attr, InstrumentedAttribute):
            raise QueryError("Invalid filter attribute '%s'" % name)
        if related is not None:
            if not isinstance(attr.property, RelationshipProperty):
                raise QueryError("Filter attribute '%s' is not related attribute" % name)
            related_class = attr.property.argument()
            attr = getattr(related_class, related, None)
            if not isinstance(attr, InstrumentedAttribute):
                raise QueryError("Related filter attribute '%s' has no attribute '%s'" % (name, related))
        return attr


class QueryFilter(QueryBase):
    """Filtering."""

    operand_separator = ':'
    group_separator = ';'
    element_separator = ','
    time_separator = '.'
    arithmetic_separator = "'"
    usable_operators = ['lt', 'le', 'eq', 'ne', 'ge', 'gt', 'in', 'notin', 'contains', 'startswith', 'endswith']
    logic_forms = dict(dnf=(or_, and_), cnf=(and_, or_))

    @staticmethod
    def _make_time(values: list):
        """w.d.h.m.s.ms, can throw ValueError."""
        t = [0] * 6
        for i, v in enumerate(values[:6]):
            v = 0 if v == '' else int(v)
            t[i] = v
        return timedelta(weeks=t[0], days=t[1], hours=t[2], minutes=t[3], seconds=t[4], milliseconds=t[5])

    @staticmethod
    def _make_date(values: list):
        """y.m.d.h.m.s, can throw ValueError."""
        t = [0] * 6
        for i, v in enumerate(values[:6]):
            v = 0 if v == '' else int(v)
            t[i] = v
        try:
            return datetime(year=t[0], month=t[1], day=t[2], hour=t[3], minute=t[4], second=t[5])
        except ValueError as err:
            raise QueryError(err.args[0])

    @classmethod
    def _parse_group(cls, group: str):
        try:
            a, o, v = group.split(cls.operand_separator, 2)
            return a, o, v
        except ValueError:
            raise QueryError("Invalid filter subgroup '%s'" % group)

    @classmethod
    def _parse_method(cls, attr, op: str):
        if op not in cls.usable_operators:
            raise QueryError("Invalid filter operator '%s'" % op)
        try:
            name = next(filter(lambda e: hasattr(attr, e % op), ['%s', '%s_', '__%s__'])) % op
            return getattr(attr, name)
        except StopIteration:
            raise QueryError('Invalid filter operator: %s' % op)

    @classmethod
    def _parse_operand(cls, model, operand: str):
        if operand.startswith('$_') and len(operand) > 2:
            if operand == '$_null':
                return None
            elif operand == '$_now':
                return datetime.utcnow()
            elif operand.startswith('$_time' + cls.time_separator):
                values = operand.split(cls.time_separator)
                values.pop(0)
                return cls._make_time(values)
            elif operand.startswith('$_date' + cls.time_separator):
                values = operand.split(cls.time_separator)
                values.pop(0)
                return cls._make_date(values)
        elif operand.startswith('$') and len(operand) > 1:
            return cls._parse_attribute(model, operand[1:])
        # That idea didn't work..
        # elif operand.startswith('@'):
        #     name, aop, value = operand[1:].split(cls.arithmetic_separator, 2)
        #     attr = cls._parse_attribute(model, name)
        #     if aop not in cls.usable_arithmetic:
        #         raise QueryError("Invalid arithmetic operator '%s'" % aop)
        #     return attr.op(value)
        return operand

    @classmethod
    def _parse_right_side(cls, model, rhs: str) -> list:
        values = rhs.split(cls.element_separator)
        return [cls._parse_operand(model, v) for v in values]

    @classmethod
    def filter(cls, query, params, option):
        if params is None or len(params) == 0:
            return query
        if option is None:
            option = 'dnf'
        primary_logic, secondary_logic = cls.logic_forms.get(option, (None, None))
        if primary_logic is None:
            raise QueryError("Invalid filter logic '%s'" % option)
        model_class = query.column_descriptions[0]['entity']
        criterion_groups = []
        for group in params:
            if group == '':
                continue
            subgroups = group.split(cls.group_separator)
            criterion_subgroups = []
            for subgroup in subgroups:
                if subgroup == '':
                    continue
                name, op, rhs = cls._parse_group(subgroup)
                attr = cls._parse_attribute(model_class, name)
                operate = cls._parse_method(attr, op)

                if op == 'in' or op == 'notin':
                    criterion = operate(rhs.split(cls.element_separator))
                else:
                    right_operands = cls._parse_right_side(model_class, rhs)
                    try:
                        criterion = operate(*right_operands)
                    except TypeError:
                        raise QueryError("Invalid filter operand count '%s'" % len(right_operands))
                criterion_subgroups.append(criterion)
            criterion = secondary_logic(*criterion_subgroups)
            criterion_groups.append(criterion)
        criterion = primary_logic(*criterion_groups)
        return query.filter(criterion)


class QuerySort(QueryBase):
    """Sorting."""

    operand_separator = ':'
    group_separator = ';'

    sort_forms = dict(asc=asc, dsc=desc)

    @classmethod
    def sort(cls, query, param: str):
        if param is None or param == '':
            return query
        model_class = query.column_descriptions[0]['entity']
        groups = param.split(cls.group_separator)
        criterions = []
        for group in groups:
            splits = group.split(cls.operand_separator, 1)
            if len(splits) == 2:
                name, order = splits
            else:
                name, order = splits[0], 'asc'
            operate = cls.sort_forms.get(order, None)
            if operate is None:
                raise QueryError("Invalid sorting form '%s'" % order)
            attr = cls._parse_attribute(model_class, name)
            criterion = operate(attr)
            criterions.append(criterion)
        return query.order_by(*criterions)
