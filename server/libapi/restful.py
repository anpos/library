"""
restful.py
- Utility module.
"""

import logging
from functools import wraps
from flask import request
from marshmallow import ValidationError
from http import HTTPStatus


api_logger = logging.getLogger('api')


class ApiResponse(dict):
    """Uniform response format."""

    def __init__(self, data, status: HTTPStatus, logger, log_message):
        logger.info(f'{request.remote_addr} - "{request.method} {request.url_rule}" {status.value} {status.name} - {log_message}')
        success = status.value < 400
        super().__init__(success=success,
                         status=f"{status.value} {status.name}",
                         message=status.description,
                         data=data)

    @property
    def flask(self):
        """Prepare for flask."""
        return self, self['status']


def handle_bad_request(e):
    """Catch those bad requests."""
    return ApiResponse(None, HTTPStatus.BAD_REQUEST, api_logger, None).flask


def consumes_json(f):
    """Decorator to consume only JSON media type."""
    @wraps(f)
    def wrapper(*args, **kwargs):
        if request.mimetype != 'application/json' and request.method not in {'GET', 'DELETE'}:
            return ApiResponse(None, HTTPStatus.UNSUPPORTED_MEDIA_TYPE, api_logger, None).flask
        return f(*args, **kwargs)
    return wrapper


def restful_response(f):
    """
    Decorator to convert route output to ApiResponse object.

    This decorator optionally accepts 'param log message' as first argument.
    Decorated function is expected to return 3 values: 'data', 'HTTPStatus', 'result log message'.
    """
    @wraps(f)
    def route_response(*args, **kwargs):
        data, status, result_message = f(*args, **kwargs)
        return ApiResponse(data, status, api_logger, result_message).flask
    return route_response


class APIError(Exception):
    """Basic error."""

    def __init__(self, message: str, *args):
        super().__init__(message, *args)

    @property
    def message(self):
        return self.args[0]


class DatabaseError(APIError):
    pass


class NotFoundError(APIError):
    pass


class AuthenticationError(APIError):
    pass


class QueryError(APIError):
    pass


def create_resource(schema, key=None):
    """Create resource."""
    data = request.get_json()
    try:
        obj = schema.load(data)
        obj = obj.create()
        return schema.dump(obj), HTTPStatus.OK, f"Created new resource: {obj.id}."
    except ValidationError as err:
        return err.messages, HTTPStatus.BAD_REQUEST, "ValidationError"
    except DatabaseError as err:
        return None, HTTPStatus.CONFLICT, err.message


def read_resource(schema, key=None):
    """Read resource."""
    try:
        if key is None:
            obj = schema.all(request.args.getlist('filter'),
                             request.args.get('filter_opt'),
                             request.args.get('sort'),
                             request.args.get('offset'),
                             request.args.get('limit'))
        else:
            obj = schema.find(key)

        return schema.dump(obj, many=(key is None)), HTTPStatus.OK, f"Queried: {key is None or key}"
    except NotFoundError as err:
        return err.message, HTTPStatus.NOT_FOUND, err.message
    except QueryError as err:
        return err.message, HTTPStatus.NOT_FOUND, err.message


def update_resource(schema, key: int):
    """Update resource."""
    data = request.get_json()
    try:
        obj = schema.find(key)
        obj = schema.load(data, instance=obj, partial=True)
        obj = obj.update()
        return schema.dump(obj), HTTPStatus.OK, f"Updated resource: {key}."
    except ValidationError as err:
        return err.messages, HTTPStatus.BAD_REQUEST, "ValidationError"
    except DatabaseError as err:
        return None, HTTPStatus.CONFLICT, err.message
    except NotFoundError as err:
        return err.message, HTTPStatus.NOT_FOUND, err.message


def delete_resource(schema, key: int):
    """Delete resource."""
    try:
        obj = schema.find(key)
        obj.delete()
        return None, HTTPStatus.NO_CONTENT, f"Deleted resource: {key}."
    except NotFoundError as err:
        return err.message, HTTPStatus.NOT_FOUND, err.message


crud_operation = {
    'GET': read_resource,
    'POST': create_resource,
    'PUT': update_resource,
    'DELETE': delete_resource
}
