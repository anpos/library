﻿"""
models.py
- Data classes for the library application.
"""


from flask import current_app
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy import func
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.event import listens_for
from sqlalchemy.exc import DBAPIError
from sqlalchemy.orm import validates
from sqlalchemy.ext.associationproxy import association_proxy
from flask_marshmallow import Marshmallow
from marshmallow import fields, validate, ValidationError, validates_schema
from marshmallow_sqlalchemy import ModelSchema
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from datetime import datetime, timedelta
from .restful import QueryError, DatabaseError, NotFoundError, AuthenticationError
from .query_utils import QueryFilter, QuerySort


db = SQLAlchemy()
ma = Marshmallow()


class R:
    """Some basic bitwise roles. Not to change NONE-value."""
    NONE = 0
    ANONYMOUS = 1
    OPERATOR = 2
    ADMIN = 4
    ANY_USER = OPERATOR | ADMIN  # can alter resources
    ANYONE = ANY_USER | ANONYMOUS


class BaseModel(db.Model):
    """Base model, includes primary key and dates."""

    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    def create(self):
        """Create record."""
        try:
            db.session.add(self)
            db.session.commit()
        except DBAPIError as err:
            raise DatabaseError(err.code, err.orig)
        return self

    def update(self):
        """Update record."""
        try:
            db.session.commit()
        except DBAPIError as err:
            raise DatabaseError(err.code, err.orig)
        return self

    def delete(self):
        """Delete record."""
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def count(cls):
        """Count records."""
        return db.session.query(cls).count()


class User(BaseModel):
    """Employee user model. Librarians and admins."""

    __tablename__ = 'users'
    
    username = db.Column(db.String(64), nullable=False, unique=True, index=True, default="")
    password_hash = db.Column(db.String(128), nullable=False, default="")
    roles = db.Column(db.Integer, nullable=False, default=R.NONE)

    @property
    def password(self):
        raise AttributeError('Password is not a readable attribute.')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    @staticmethod
    def verify_token(auth_token):
        """Verify token and return user object."""
        try:
            payload = jwt.decode(auth_token, current_app.config.get('SECRET_KEY'))
            user = User.query.get(payload['sub'])
            if user is None:
                raise AuthenticationError("Token user not found")
            return user
        except jwt.ExpiredSignatureError:
            raise AuthenticationError("Token signature expired. Please log in again.")
        except jwt.InvalidTokenError:
            raise AuthenticationError("Invalid token. Please log in again.")

    @staticmethod
    def verify_credentials(username, password):
        """Verify credentials and return user object."""
        user = User.query.filter_by(username=username).first()
        if user is None:
            raise AuthenticationError("Username does not exist")
        if not check_password_hash(user.password_hash, password):
            raise AuthenticationError("Password does not match")
        return user

    @staticmethod
    def issue_token(user_id):
        """Generate auth token. Might raise some standard exception."""
        token_lifetime = current_app.config.get('API_TOKEN_LIFETIME', 600)
        payload = {
            'exp': datetime.utcnow() + timedelta(seconds=token_lifetime),
            'iat': datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(payload, current_app.config.get('SECRET_KEY'), algorithm='HS256')

    @property
    def as_logged(self) -> str:
        return f"Related user: {self.id} ({self.username})"


class Publisher(BaseModel):
    """Some information about book publisher."""

    __tablename__ = 'publishers'

    name = db.Column(db.String(100), nullable=False)

    books = db.relationship("Book", back_populates="publisher")


class BookAuthor(BaseModel):
    """Book-Author many-to-many relationship model."""

    __tablename__ = 'bookauthors'

    book_id = db.Column(db.Integer, db.ForeignKey('books.id'), nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('authors.id'), nullable=False)

    book = db.relationship("Book", backref="author_associations")
    author = db.relationship("Author", backref="book_associations")

    __table_args__ = (db.Index('ux_bookauthor', 'book_id', 'author_id', unique=True),)


class BookBorrower(BaseModel):
    """Book-Borrower many-to-many relationship model."""

    __tablename__ = 'bookborrowers'

    book_id = db.Column(db.Integer, db.ForeignKey('books.id'), nullable=False)
    borrower_id = db.Column(db.Integer, db.ForeignKey('borrowers.id'), nullable=False)
    deadline_on = db.Column(db.DateTime, default=lambda: datetime.utcnow() + timedelta(days=28))
    returned_on = db.Column(db.DateTime)

    book = db.relationship("Book", backref="borrower_associations")
    borrower = db.relationship("Borrower", backref="book_associations")

    __table_args__ = (db.Index('ux_bookborrower', 'book_id', 'borrower_id', unique=True),)

    @property
    def returned(self):
        return self.returned_on is not None

    @returned.setter
    def returned(self, is_returned):
        if is_returned and self.returned_on is None:
            self.returned_on = datetime.utcnow()


@listens_for(BookBorrower, 'before_insert')
def set_deadline(mapper, connect, self):
    borrow_time = timedelta(days=current_app.config.get('API_STANDARD_BORROW_TIME', 60))
    book = Book.query.get(self.book_id)
    if book is not None:
        copies_min = current_app.config.get('API_MIN_BOOK_COPIES', 5)
        age_min = timedelta(days=current_app.config.get('API_MIN_BOOK_AGE', 6) * 30)
        copies = book.copies_available()
        if copies < copies_min or datetime.utcnow() < book.created_on + age_min:
            borrow_time = timedelta(days=current_app.config.get('API_SHORTENED_BORROW_TIME', 30))
    self.deadline_on = datetime.utcnow() + borrow_time


class Author(BaseModel):
    """Some information about book author."""

    __tablename__ = 'authors'

    first_name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)

    books = association_proxy("book_associations", "book", creator=lambda x: BookAuthor(book=x))


class Borrower(BaseModel):
    """Should contain some contact information."""

    __tablename__ = 'borrowers'

    first_name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    address = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False)

    books = association_proxy("book_associations", "book", creator=lambda x: BookBorrower(book=x))


class Book(BaseModel):
    """Library book information."""

    __tablename__ = 'books'

    title = db.Column(db.String(100), nullable=False)
    desc = db.Column(db.String(500))
    genre = db.Column(db.String(100))
    publisher_id = db.Column(db.Integer, db.ForeignKey('publishers.id'), nullable=False)
    lang_id = db.Column(db.Integer)
    year = db.Column(db.Integer)
    copies = db.Column(db.Integer, nullable=False, default=0)

    publisher = db.relationship("Publisher", back_populates="books")

    authors = association_proxy("author_associations", "author", creator=lambda x: BookAuthor(author=x))
    borrowers = association_proxy("borrower_associations", "borrower", creator=lambda x: BookBorrower(borrower=x))

    def copies_available(self):
        return self.copies - BookBorrower.query.filter(BookBorrower.book_id == self.id).count()


"""
- Marshmallow schemas with validation data, for serialization and deserialization
"""


class BaseSchema(ModelSchema):
    id = fields.Integer(dump_only=True)
    created_on = fields.DateTime(dump_only=True)
    updated_on = fields.DateTime(dump_only=True)

    class Meta:
        # model = BaseModel
        sqla_session = db.session
        ordered = True

    @classmethod
    def all(cls, filter_groups, filter_opt, sort_group, offset, limit):
        """Get all records matching criterion."""
        query = db.session.query(cls.Meta.model)
        query = QueryFilter.filter(query, filter_groups, filter_opt)
        query = QuerySort.sort(query, sort_group)
        # TODO: paging, that should also return header Content-Range
        return query.all()

    @classmethod
    def find(cls, key):
        """Find record, raise DatabaseError if not found."""
        obj = cls.Meta.model.query.get(key)
        if obj is None:
            raise NotFoundError(f"Resource {key} does not exist.")
        return obj

    @classmethod
    def find_first_by(cls, **kwargs):
        """Find record by attribute."""
        return cls.Meta.model.query.filter_by(**kwargs).first()


class PublisherSchema(BaseSchema):
    name = fields.Str(required=True, validate=[validate.Length(min=3, max=250)])

    books = fields.Nested(lambda: BookSchema, many=True, only={'id', 'title'}, dump_only=True)

    class Meta(BaseSchema.Meta):
        model = Publisher


class AuthorSchema(BaseSchema):
    first_name = fields.Str(required=True, validate=[validate.Length(min=1, max=250)])
    last_name = fields.Str(required=True, validate=[validate.Length(min=2, max=250)])

    books = fields.Nested(lambda: BookSchema, many=True, only={'id', 'title'}, dump_only=True)

    class Meta(BaseSchema.Meta):
        model = Author
        exclude = ('book_associations',)


class BorrowerSchema(BaseSchema):
    first_name = fields.Str(required=True, validate=[validate.Length(min=1, max=250)])
    last_name = fields.Str(required=True, validate=[validate.Length(min=2, max=250)])
    address = fields.Str(required=True, validate=[validate.Length(min=3, max=250)])
    phone = fields.Str(required=True, validate=[validate.Length(min=4, max=250)])
    email = fields.Str(required=True, validate=[validate.Email()])

    books = fields.Nested(lambda: BookSchema, many=True, only={'id', 'title'}, dump_only=True)

    class Meta(BaseSchema.Meta):
        model = Borrower
        exclude = ('book_associations',)


class BookSchema(BaseSchema):
    title = fields.Str(required=True, validate=[validate.Length(min=1, max=100)])
    publisher_id = fields.Integer(required=True, load_only=True)
    desc = fields.Str(required=False, validate=[validate.Length(min=0, max=500)])
    genre = fields.Str(required=False, validate=[validate.Length(min=0, max=50)])
    lang_id = fields.Integer(required=False, validate=[validate.Range(min=0, max=300)])
    year = fields.Integer(required=False, validate=[validate.Range(min=1400, max=datetime.today().year)])
    copies = fields.Integer(required=False, validate=[validate.Range(min=0)])
    copies_available = fields.Function(lambda obj: obj.copies_available())

    publisher = fields.Nested(PublisherSchema, only={'id', 'name'}, dump_only=True)
    authors = fields.Nested(AuthorSchema, many=True, only={'id', 'first_name', 'last_name'}, dump_only=True)
    borrowers = fields.Nested(BorrowerSchema, many=True, only={'id', 'first_name', 'last_name'}, dump_only=True)

    class Meta(BaseSchema.Meta):
        model = Book
        exclude = ('author_associations', 'borrower_associations')


class BookAuthorSchema(BaseSchema):
    book_id = fields.Integer(required=True, load_only=True)
    author_id = fields.Integer(required=True, load_only=True)

    book = fields.Nested(BookSchema, only={'id', 'title'}, dump_only=True)
    author = fields.Nested(AuthorSchema, only={'id', 'first_name', 'last_name'}, dump_only=True)

    class Meta(BaseSchema.Meta):
        model = BookAuthor


class BookBorrowerSchema(BaseSchema):
    book_id = fields.Integer(required=True, load_only=True)
    borrower_id = fields.Integer(required=True, load_only=True)
    deadline_on = fields.DateTime(dump_only=True)
    returned_on = fields.DateTime(dump_only=True)
    returned = fields.Boolean()

    book = fields.Nested(BookSchema, only={'id', 'title'}, dump_only=True)
    borrower = fields.Nested(AuthorSchema, only={'id', 'first_name', 'last_name'}, dump_only=True)

    class Meta(BaseSchema.Meta):
        model = BookBorrower

    @validates_schema(skip_on_field_errors=False)
    def validate_stock(self, data, **options):
        book_id = data.get('book_id', None)
        if book_id is not None:
            book = Book.query.get(book_id)
            if book is not None and book.copies_available() < 1:
                raise ValidationError('Out of stock', 'book_id')


class UserSchema(BaseSchema):
    username = fields.Str(required=True, validate=[validate.Length(min=4, max=30)])
    password = fields.Str(required=True, validate=[validate.Length(min=6, max=30)])
    roles = fields.Integer(required=True, validate=[validate.Equal(R.ADMIN)])

    class Meta(BaseSchema.Meta):
        model = User


"""
- Marshmallow schema objects.
"""

users_schema = UserSchema(exclude={'password_hash', 'roles'})
users_schema_first_time = UserSchema(exclude={'password_hash'})
authors_schema = AuthorSchema()
publishers_schema = PublisherSchema()
borrowers_schema = BorrowerSchema()
books_schema = BookSchema()
bookborrowers_schema = BookBorrowerSchema()
bookauthors_schema = BookAuthorSchema()
books_schema_no_borrowers = BookSchema(exclude={'borrowers'})
