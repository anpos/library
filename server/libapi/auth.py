"""
auth.py
- Authentication api.
"""

from werkzeug.http import parse_authorization_header
from flask import Blueprint
from .restful import *
from .models import *


auth = Blueprint('auth', __name__)


def authorize(default, **kwroles):
    """
    Broad restrictions.

    If authentication and authorization is successful, pass 'auth_message' and authorized 'user' as first 2 arguments.
    If anyone is authorized, pass None as 'user'.
    """
    def decorator(f):
        @wraps(f)
        def route_authorize(*args, **kwargs):
            permissible_roles = kwroles.get(request.method, default)
            try:
                user = retrieve_token_user()
                user_roles = user.roles
                data = None
                related = user.as_logged
                message = ''
            except AuthenticationError as err:
                user = None
                user_roles = R.ANONYMOUS
                data = err.message
                related = ''
                message = err.message
            if (permissible_roles & user_roles) == R.NONE:
                status = HTTPStatus.UNAUTHORIZED if user is None else HTTPStatus.FORBIDDEN
                return data, status, f'{message} - {related}'
            data, status, message = f(user, *args, **kwargs)
            return data, status, f'{message} - {related}'
        return route_authorize
    return decorator


def retrieve_token_user():
    """Get auth token from request header. Throws AuthenticationError."""
    auth_header = request.headers.get('Authorization')
    if auth_header is None:
        raise AuthenticationError("Missing authorization header")
    try:
        auth_token = auth_header.split(" ")[1]
    except IndexError:
        raise AuthenticationError("Invalid authorization header")
    return User.verify_token(auth_token)


def retrieve_credentials_user():
    """Get auth credentials from request header. Throws AuthenticationError."""
    auth_header = request.headers.get('Authorization')
    if auth_header is None:
        raise AuthenticationError("Missing authorization header")
    auth_info = parse_authorization_header(auth_header)
    if auth_info is None or auth_info.password is None:
        raise AuthenticationError("Invalid authorization header or not supported authorization method")
    return User.verify_credentials(auth_info.username, auth_info.password)


@auth.route('/token/', methods=['GET', 'POST'])
@restful_response
def login_user():
    try:
        user = retrieve_credentials_user()
    except AuthenticationError as err:
        return err.message, HTTPStatus.UNAUTHORIZED, err.message
    token = User.issue_token(user.id)
    return token.decode(), HTTPStatus.OK, f"Token issued. {user.as_logged}"


@auth.route('/register/', methods=['POST'])
@restful_response
def register_user():
    if User.count() == 0:
        return create_resource(users_schema_first_time)
    return create_resource(users_schema)


@auth.route('/edit/<int:user_id>', methods=['PUT'])
@restful_response
@authorize(R.ANY_USER)
def update_user(current_user: User, user_id):
    if current_user.roles != R.ADMIN and user_id != current_user.id:
        return None, HTTPStatus.FORBIDDEN, None
    return update_resource(users_schema, user_id)


@auth.route('/unregister/<int:user_id>', methods=['DELETE'])
@restful_response
@authorize(R.ANY_USER)
def delete_user(current_user: User, user_id):
    if current_user.roles != R.ADMIN and user_id != current_user.id:
        return None, HTTPStatus.FORBIDDEN, None
    return delete_resource(users_schema, user_id)


@auth.route('/users/', methods=['GET'])
@auth.route('/users/<int:user_id>', methods=['GET'])
@restful_response
@authorize(R.ANY_USER)
def get_users(current_user, user_id=None):
    return read_resource(users_schema, user_id)
